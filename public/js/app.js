(function (){
	var app = angular.module('app', ['ui.bootstrap','directives']);

	app.controller('SearchController', function($http) {

		var This = this
		this.error = []
		this.tweets = []
		this.past = []
		this.message = {
			home:{header:"Dobrodošli na stranicu!",text:"Ovdje možete pretraživati tweet-ove..."},
			nothingFound:{header:"Nema ništa!",text:"Žao nam je ali nismo pronašli to što ste tražili..."}
		}

		this.Pagination = function (){

			this.currPage = 1
			this.itemsPerPage = 5

			this.GetPage = function () {
				return this.currPage
			}

			this.GetSlicedTweets = function (){
				return This.tweets.slice((this.currPage-1) * this.itemsPerPage,this.currPage * this.itemsPerPage)
			}

			this.SetPage = function (page){
				this.currPage = page;
			}

			this.NextPage = function (){
				this.currPage ++
				if (this.currPage >= this.PagesArray().length) this.currPage = this.PagesArray().length
			}

			this.PrevPage = function (){
				this.currPage --
				if (this.currPage <=1) this.currPage = 1
			}

			this.PagesArray = function () {
				return (((b=[]).length=This.tweets.length/this.itemsPerPage+This.tweets.length%this.itemsPerPage)&&b)
			}
		}

		this.GetTweets = function (){

			this.past.push({text:this.value})

			$http.get('/api/search/'+this.value).
			success(function(data, status, headers, config) {
				This.ErrorCheck(data,status)
				if (!This.error.length)	This.tweets = data.statuses
			}).
			error(function(data, status, headers, config) {
				This.ErrorCheck(data,status)
			});

		}

		this.ErrorCheck = function (data,status){

			this.error = []

			if (typeof data.statuses !== 'undefined'){
				if (!data.statuses.length) this.error = [this.message.nothingFound]
				return ;
			}

			if (typeof data.errors !== 'undefined'){

				if (data.errors[0].code > 0) {

					message = {
						header:"Code: " + data.errors[0].code,
						text:"Message: " + data.errors[0].message
					}
					this.error = [message]

				}

				return ;
			}

		}

		// On page load

		this.pagination = new this.Pagination()
		this.error = [this.message.home]

	})
	.filter('highlight', function($sce) {
	    return function(text, phrase) {
	      	if (phrase) text = text.replace(new RegExp('('+phrase+')', 'gi'),
	        	'<span class="hightlight">$1</span>')

	      	return $sce.trustAsHtml(text)
	    }
  	})
	
})()
