(function (){
	var app = angular.module('directives', []);

	app.directive("searchBar", function() {
	    return {
	      restrict: 'E',
	      templateUrl: "../components/search-bar.html"
	    };
  	});

  	app.directive("sideBar", function() {
	    return {
	      restrict: 'E',
	      templateUrl: "../components/side-bar.html"
	    };
  	});

  	app.directive("searchResults", function() {
	    return {
	      restrict: 'E',
	      templateUrl: "../components/search-results.html"
	    };
  	});

})()