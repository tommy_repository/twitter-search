module.exports = function(app) {

        app.get(["/api/search/:query", '/api/search/'], require("./api/twitterSearch.js"))

        app.get('*', function(req, res) {
            res.sendfile('./public/views/index.html');
        });

    };